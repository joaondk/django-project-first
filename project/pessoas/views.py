
from django.shortcuts import render, redirect
from django.views.generic import ListView, CreateView, DeleteView
from django.urls import reverse_lazy
from .forms import PessoaForm
from .models import Pessoa

#def listar_pessoas(request):
#    pessoas = Pessoa.objects.all()
#    return render(request, 'pessoas/listar_pessoas.html', { 'pessoas': pessoas })

class PessoaListView(ListView):
    model = Pessoa
    template_name = 'pessoas/listar_pessoas.html'


class PessoaCreateView(CreateView):
    model = Pessoa
    template_name = 'pessoas/criar_pessoas.html'
    form_class = PessoaForm
    success_url = reverse_lazy('pessoas_pessoas_listar')

#def criar_pessoas(request):
#    if request.method == 'GET':
#        return render(request, 'pessoas/criar_pessoas.html')
#    else:
#        nome = request.POST['nome']
#        idade = request.POST['idade']
#        if (int(idade) < 0):
#            return render(request, 'pessoas/criar_pessoas.html', {'erro': 'idade nao pode ser menor que zero'})
#        pessoa = Pessoa(nome=nome, idade=idade)
#        pessoa.save()
#        return redirect('/pessoas')
