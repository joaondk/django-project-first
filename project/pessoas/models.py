from django.db import models

# MODELO QUE REPRESENTARÁ UMA TABELA NO BANCO DE DADOS
# CADA CRIAÇÃO/ALTERAÇÃO AQUI DEVEMOS GERAR UM NOVO COMPILADO PARA AVISAR AO BANCO DE DADOS, UTILIZANDO
# python manage.py makemigratios
# python manage.py migrate

class Pessoa(models.Model):
    nome = models.CharField(max_length=50)
    idade = models.IntegerField()

def __str__(self):
    return self.nome