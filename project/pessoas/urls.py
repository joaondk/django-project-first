from django.urls import path
from .views import PessoaListView, PessoaCreateView

# CAMINHOS DA URL (PÁGINA/SITE A IR) RELACIONADAS AS VIEWS

urlpatterns = [
    path('', PessoaListView.as_view(), name='pessoas_pessoas_listar'),
    path('criar/', PessoaCreateView.as_view(), name='pessoas_pessoas_criar'),
]
